# Sweet vim plugins and stuff


### Requirements

* vim, and vim-pathogen;
* Have your user in sudoers;
* Basic linux shell commnads.


### Install
---
#### Git:

```bash
    carcara@sanguinolento[~/]$: git clone https://gitlab.com/fredericosales/sweet_vim.git
```

#### To install vim, vim-pathogen, .vimrc, and .vim/
```bash
    carcara@sanguinolento[~/]$: cd sweet_vim/ && bash install -i
```

#### To install .vimrc, and .vim/
```bash
    carcara@sanguinolento[~/]$: cd sweet_vim/ && bash install -p
```

### Enjoy
---

![vim](vim.png)